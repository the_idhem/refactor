package domain;

import java.util.List;

import domain.exceptions.EnrollmentRulesViolationException;

public class EnrollCtrl {
    public void enroll(Student student, List<Offering> offers) throws EnrollmentRulesViolationException {

        for (Offering offer : offers) {

            student.checkStudentPassPreRequire(offer);

            student.courseNotPassedByStudent(offer);

            offer.checkInconsistencyOnExamTime(offers);

            offer.checkNotRepeatedCourse(offers);
        }
        student.checkGpaWithNumberOfUnitCourse(offers);
    }


}
