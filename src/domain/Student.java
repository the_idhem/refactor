package domain;

import domain.exceptions.EnrollmentRulesViolationException;

import java.util.ArrayList;
import java.util.List;

public class Student {
    private String id;
    private String name;

    private List<StudyRecord> studyRecords;

    public Student(String id, String name) {
        this.id = id;
        this.name = name;
        this.studyRecords = new ArrayList<StudyRecord>();
    }

    public void takeOffering(Offering c) {
        getStudyRecords().add(new StudyRecord(c));
    }

    public void takeOffering(Offering c, double grade) {
        getStudyRecords().add(new StudyRecord(c, grade));
    }

    public List<StudyRecord> getStudyRecords() {
        return studyRecords;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }

    public void checkStudentPassPreRequire(Offering offer) throws EnrollmentRulesViolationException {
        List<Course> prereqs = offer.getCourse().getPrerequisites();
        nextPre:
        for (Course pre : prereqs) {
            List<StudyRecord> transcript = this.getStudyRecords();
            for (StudyRecord sr : transcript) {
                if (sr.getOffering().getCourse().equals(pre) && sr.getGrade() >= 10)
                    continue nextPre;
            }
            throw new EnrollmentRulesViolationException(String.format("The student has not passed %s as a prerequisite of %s", pre.getName(), offer.getCourse().getName()));
        }


    }

    public void courseNotPassedByStudent(Offering offer) throws EnrollmentRulesViolationException {
        List<StudyRecord> transcript = this.getStudyRecords();
        for (StudyRecord sr : transcript) {
            if (sr.getOffering().getCourse().equals(offer.getCourse()) && sr.getGrade() >= 10)
                throw new EnrollmentRulesViolationException(String.format("The student has already passed %s", offer.getCourse().getName()));
        }
    }

    public void checkGpaWithNumberOfUnitCourse(List<Offering> offers) throws EnrollmentRulesViolationException {
        int unitsRequested = 0;
        for (Offering o : offers)
            unitsRequested += o.getUnits();
        double points = 0;
        int totalUnits = 0;
        for (StudyRecord sr : this.getStudyRecords()) {
            points += sr.getGrade() * sr.getUnits();
            totalUnits += sr.getUnits();
        }
        double gpa = points / totalUnits;
        if ((gpa < 12 && unitsRequested > 14) ||
                (gpa < 16 && unitsRequested > 16) ||
                (unitsRequested > 20))
            throw new EnrollmentRulesViolationException(String.format("Number of units (%d) requested does not match GPA of %f", unitsRequested, gpa));
        for (Offering o : offers)
            this.takeOffering(o);
    }
}
