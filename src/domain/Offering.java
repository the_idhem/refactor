package domain;
import domain.exceptions.EnrollmentRulesViolationException;

import java.util.Date;
import java.util.List;

public class Offering {
	private String id;
	private Course course;
	private int section;
	private Date examDate;
	private Term term;
	
	public Offering(Course course, Term term) {
		this.course = course;
		this.section = 1;
		this.examDate = null;
		this.term = term;
	}

	public Offering(Course course, Term term, Date examDate) {
		this.course = course;
		this.section = 1;
		this.examDate = examDate;
		this.term = term;
	}

	public Offering(String id, Course course, Term term, Date examDate, int section) {
		this.id = id;
		this.course = course;
		this.section = section;
		this.examDate = examDate;
		this.term = term;
	}
	
	public Course getCourse() {
		return course;
	}
	
	public String toString() {
		return id + ":\t" + course.getName() + " - " + section + " [" + term + "]";
	}
	
	public Date getExamTime() {
		return examDate;
	}

	public int getUnits() {
		return course.getUnits();
	}
	
	public String getId() {
		return id;
	}
	
	public Term getTerm() {
		return term;
	}

	public void checkInconsistencyOnExamTime(List<Offering> offers) throws EnrollmentRulesViolationException {
		for (Offering o2 : offers) {
			if (this == o2)
				continue;
			if (this.getExamTime().equals(o2.getExamTime()))
				throw new EnrollmentRulesViolationException(String.format("Two offerings %s and %s have the same exam time", this, o2));
		}
	}

	public void checkNotRepeatedCourse(List<Offering> offers) throws EnrollmentRulesViolationException {
		for (Offering o2 : offers) {
			if (this == o2)
				continue;
			if (this.getCourse().equals(o2.getCourse()))
				throw new EnrollmentRulesViolationException(String.format("%s is requested to be taken twice", this.getCourse().getName()));
		}
	}
}
